#!/usr/bin/python
import sys

def parse_file(path, data):
  with open(path) as f:
    links_to_next = list()
    for line in f:
      if line[0] == '\t':
        # Add to the list of sites which will link to the next target
        line = line.strip()
        assert('\t' not in line)
        links_to_next.append(line)
      else:
        # Found a target
        line = line.strip()
        parts = line.split()
        assert len(parts) == 2, line
        page = parts[0]
        count = int(parts[1])
        refs = data.get(page, list())
        refs.extend(links_to_next * count)
        data[page] = refs
        links_to_next = list()
  return data

def save_results(out_path, data):
  with open(out_path, 'w') as f:
    for page, ref in data.iteritems():
      assert(' ' not in page)
      assert(' ' not in ref)
      assert('\t' not in page)
      assert('\t' not in ref)
      f.write('{} {}\n'.format(page, ref))

def main():
  # Check args
  if len(sys.argv) < 3:
    print('Usage: {} out-file.txt in-file1.txt [in-file2.txt [...]]'.format(sys.argv[0]))
    sys.exit(1)
  # Run on each file
  data = dict()
  for in_file in sys.argv[2:]:
    data = parse_file(path=in_file, data=data)
  # Dump results into a file
  out_file = sys.argv[1]
  save_results(out_path=out_file, data=data)
  print('Results saved to {}'.format(out_file))

if __name__ == '__main__':
  main()


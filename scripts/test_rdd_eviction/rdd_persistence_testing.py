from pyspark.sql import SparkSession
import pyspark
import sys

def print_usage_and_exit():
  print('Usage: %s [num_random_files]' % sys.argv[0])
  print('    num_random_files: 1-15. Files are 10MiB each.')
  sys.exit(1)

def main():
  # Parse args
  if len(sys.argv) >= 3:
    print_usage_and_exit()
  elif len(sys.argv) == 2:
    try:
      num_data = int(sys.argv[1])
    except:
      print_usage_and_exit()
    if num_data <= 0 or num_data > 15:
      print_usage_and_exit()
  else:
    num_data = 15

  spark = SparkSession\
      .builder\
      .appName("Test RDD Eviction PyApp (%d)" % num_data)\
      .getOrCreate()
  randomData = list()
  for i in range(1, num_data + 1):
    randomData.append(spark.read.text('/home/jloew2/big-data/random-%d.bin' % i).rdd)
  for r in randomData:
    r.persist()
  unionRdd = reduce(lambda a, b: a.union(b), randomData)
  unionRdd.collect()
  spark.stop()

if __name__ == '__main__':
  main()


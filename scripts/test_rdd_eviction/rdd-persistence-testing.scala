// Created by Justin Loew and Stephen Skeirik
// 5/3/18
package edu.illinois.cs525.g07.spark

import scala.collection.mutable.ListBuffer
import org.apache.spark.{SparkContext, SparkConf}

object RddPersistenceTestingApp {
  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("RDD Persistence Testing Application")
    val sc = new SparkContext(conf)

    var randomData = ListBuffer[org.apache.aprk.rdd.RDD[String]]()
    for (i <- 1 to 15) {
      randomData += sc.textFile(s"/home/jloew2/big-data/random-$i.bin").cache
    }
    //for (r <- randomData) {
    //  r.persist()
    //}
    var unionRdd = randomData.reduce(_.union(_))
    unionRdd.collect()
    println(s"I am success!")
  }
}


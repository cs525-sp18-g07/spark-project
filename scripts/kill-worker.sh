#!/bin/sh
set -ex

function usage {
  echo "Usage: $0 dd"
  echo "  (where ``dd'' is the 2-digit machine identifier of the target)"
  exit 1
}

# Check args
[ "$#" -eq 1 ] || usage
target_num=$1
SPARK_PATH=~/spark

target_hostname=sp18-cs525-g07-"$target_num".cs.illinois.edu
killbot_cmd='pkill -f org.apache.spark.deploy.worker.Worker'
#resurrection_cmd="$SPARK_PATH/sbin/start-slave.sh"
resurrection_cmd=""
ssh "$target_hostname" "$killbot_cmd; $resurrection_cmd"


#!/bin/sh
set -ex

SPARK_PATH=~/spark
SCRIPT_BIN_PATH="$(dirname $0)"
# Launch the job
log_file="benchmark-$(date +%F-%T).out"
$SPARK_PATH/bin/run-example SparkPageRank ~/spark-project/datasets/pagerank_benchmark_data_www.bham.ac.uk_31245.txt 1000 2>&1 & #| tee "$log_file" &

# Wait 30 seconds, then kill a worker
sleep 30
# Unlucky number 13...
echo "Killing 13 right meow"
"$SCRIPT_BIN_PATH"/kill-worker.sh 13

# Wait 15 seconds, then kill another worker
sleep 15
# Unlucky number 12...
echo "Killing 12 right meow"
"$SCRIPT_BIN_PATH"/kill-worker.sh 12

# Wait 15 seconds, then kill another worker
sleep 15
# Unlucky number 14...
echo "Killing 14 right meow"
"$SCRIPT_BIN_PATH"/kill-worker.sh 14


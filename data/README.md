
## `final-eviction-data-10MB`
Tests were performed using a cluster containing 5 workers, each with 1024 MB of memory allocated and 1 core. 15 files were used, each containing 10MB of pseudo-random data from `/dev/urandom`. 1 run was performed and discarded as a warm-up, then 5 runs were performed. The time taken was measured by retrieving the value from the web status page; the average of the 5 runs is reported. The number of cache evictions is the sum of the number of evictions on each worker node, as reported in the logs.

`final-eviction-data-lru.csv` was obtained at commit `21adfaca24fd26b486e7dc9eb96b45674a287a30`.
`final-eviction-data-smallestfirst.csv` was obtained at commit `3c7bac610900edd724bece2e074d6107aa921f17`.

## `final-eviction-data-10MB`
Same as above, but instead of 15 files of 10MB each, there are 3 files each of size 1kB, 100kB, 1MB, 10MB, and 100MB.

## `*-kill1-*`
Same as above, but we also kill 1 worker node. We submit the job to the master, wait 15 seconds, then kill a predetermined node. The script we use kills the same node each time. The node is killed by establishing an SSH connection then killing the process. The killed node is revived between test runs.

